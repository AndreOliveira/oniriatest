AssetStore

- Particle: Unity Particle Pack (FlameStream & PressurisedSteam -> adaptei para o projeto)
- Particle: Standard Assets (Unity -> Duststorm -> adaptei para o projeto)
- Environment: Standard Assets (arvores, grama e pintura do terreno)


Freesounds

- Rocket Engines (creative commons 0 - https://freesound.org/people/qubodup/sounds/146770/)
- Beep (creative commons 0 - https://freesound.org/people/florianreichelt/sounds/459992/)
