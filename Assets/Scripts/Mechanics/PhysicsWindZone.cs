﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsWindZone : MonoBehaviour
{
	[SerializeField] private Vector3 strength = Vector3.zero;
	public List<Rigidbody> bodies {get; private set; }
	public static PhysicsWindZone instance { get; private set; }

    private void Awake()
    {
		bodies = new List<Rigidbody>();
		PhysicsWindZone.instance = this;
    }

    private void FixedUpdate()
    {
		for (int i = 0; i < bodies.Count; i++)
		{
			bodies[i].AddForce(strength);
		}
    }

}
