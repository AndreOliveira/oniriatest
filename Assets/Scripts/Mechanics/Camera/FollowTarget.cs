﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
	[SerializeField] private Transform target = null;
	[SerializeField] private float smooth = 0;
	[SerializeField] private Vector3 offset = Vector3.zero;

	private Vector3 velocity;

	private void Start() {
		velocity = Vector3.zero;
	}
	
    private void FixedUpdate()
    {
		Vector3 destination = target.position + offset;
        transform.position = Vector3.SmoothDamp(transform.position,destination, ref velocity, smooth);
		transform.LookAt(target);
    }
}
