﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody), typeof (Animator), typeof (AudioSource))]
public class Rocket : MonoBehaviour {
	#region properties
	[SerializeField] private Rigidbody parachuteRB = null;
	[SerializeField] private ScriptableFloat fuelDuration = null;
	[SerializeField] private ScriptableFloat maxHeight = null;
	[SerializeField] private ScriptableGameEvent fuelEvent = null;
	[SerializeField] private ScriptableGameEvent heightEvent = null;
	[SerializeField] private float propulsorForce = 0f;
	[SerializeField] private float parachuteDrag = 0f;

	private Animator anim;
	private PropulsorFX propulsor;
	private Rigidbody rb;
	private RocketCompartment fuelCompartment;
	private bool usingParachute;
	#endregion

	#region Monobehaviour Methods
	private void Start () {
		// fill component properties
		anim = GetComponent<Animator> ();
		rb = GetComponent<Rigidbody> ();
		fuelCompartment = GetComponentInChildren<RocketCompartment> ();

		// random start rotation
		float x = Random.Range (-60f, -120f);
		float y = Random.Range (0f, 360f);
		float z = Random.Range (0f, 360f);
		rb.MoveRotation (Quaternion.Euler (x, y, z));

		// get propulsor components
		AudioSource engineSound = GetComponent<AudioSource> ();
		ParticleSystem[] enginesFX = GetComponentsInChildren<ParticleSystem> ();

		// initial values
		maxHeight.value = 0f;
		usingParachute = false;
		rb.constraints = RigidbodyConstraints.FreezeRotation;
		propulsor = new PropulsorFX (enginesFX, engineSound);
	}

	private void OnCollisionEnter (Collision other) {
		if (other.gameObject.CompareTag ("Ground")) {
			parachuteRB.drag = 0f;
			rb.drag = 0f;
		}
	}
	#endregion

	#region  Coroutines
	private IEnumerator FirstPropulsion () {
		fuelDuration.value = fuelDuration.InitialValue;

		//first propulsion
		StartCoroutine (CheckHeight ());
		yield return Propulsion ();

		//detach second stage
		rb.constraints = RigidbodyConstraints.None;
		fuelCompartment.DetachFromRocket (transform.parent);
		yield return new WaitForSeconds (1f);

		StartCoroutine (FinalPropulsion ());
	}

	private IEnumerator Propulsion () {
		while (fuelDuration.value > 0) {
			yield return new WaitForFixedUpdate ();

			// add propulsion
			rb.AddForce (transform.forward * propulsorForce, ForceMode.Force);

			// use fuel
			fuelDuration.value -= Time.fixedDeltaTime;
			fuelEvent.Call ();
		}

		//garantee that has no fuel
		fuelDuration.value = 0f;
		fuelEvent.Call ();
	}

	private IEnumerator FinalPropulsion () {
		//recharge fuel UI for new compartment
		yield return FillFuel ();

		//active engines fx and active rocket nose propulsion
		propulsor.ActiveEngine ();
		yield return Propulsion ();

		//disable engines and lift adrift until open the parachute
		yield return propulsor.DisableEngine ();
		StartCoroutine (UpdateRotation ());
		StartCoroutine (PrepareParachute ());
	}

	private IEnumerator CheckHeight () {
		//wait until the rocket leaves the ground
		while (rb.velocity.y <= 0f) yield return new WaitForFixedUpdate();

		//get the current height as initial
		float initialHeight = transform.position.y;

		//while rocket is arising update the new max height
		while (rb.velocity.y > 0) {
			if ((transform.position.y - initialHeight) > maxHeight.value) {
				maxHeight.value = transform.position.y - initialHeight;
				heightEvent.Call ();
			}
			yield return null;
		}
	}

	private IEnumerator FillFuel () {
		// calculate a rate to complete the fuel in one second
		float rate = Time.deltaTime * fuelDuration.InitialValue;

		// fill fuel and update UI
		while (fuelDuration.value < fuelDuration.InitialValue) {
			yield return new WaitForFixedUpdate ();
			fuelDuration.value = Mathf.Clamp (fuelDuration.value + rate, 0f, fuelDuration.InitialValue);
			fuelEvent.Call ();
		}

		// fix any possible error to UI
		fuelDuration.value = fuelDuration.InitialValue;
		fuelEvent.Call ();
	}

	private IEnumerator PrepareParachute () {
		//wait until the rocket starts to looking down
		while (transform.forward.y > 0f) {
			yield return new WaitForFixedUpdate();
		}

		// open parachute
		usingParachute = true;
		anim.SetTrigger ("OpenParachute");
	}

	private IEnumerator UpdateRotation () {
		// turn the face to same velocity direction
		while (!usingParachute) {
			Quaternion rot = Quaternion.Lerp (rb.rotation, Quaternion.LookRotation (rb.velocity), Time.fixedDeltaTime);
			rb.MoveRotation (rot);
			yield return new WaitForFixedUpdate ();
		}
	}
	#endregion

	#region public methods
	public void Launch () {
		StartCoroutine (FirstPropulsion ());
	}

	public void ActiveParachuteDrag () {
		rb.drag = parachuteDrag;
	}
	#endregion

}