﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RocketCompartment : MonoBehaviour {
	
	private PropulsorFX propulsor;
	private FixedJoint joint;

	private void Start () {
		joint = GetComponent<FixedJoint> ();

		AudioSource engineSound = GetComponent<AudioSource> ();
		ParticleSystem[] enginesFX = GetComponentsInChildren<ParticleSystem> ();
		propulsor = new PropulsorFX(enginesFX, engineSound);
	}

	public void ActiveEngine () {
		propulsor.ActiveEngine();
	}

	public void DetachFromRocket (Transform newParent) {
		transform.parent = null;
		StartCoroutine(propulsor.DisableEngine());
		Destroy (joint);
	}

	
}