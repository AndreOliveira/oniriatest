﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RocketLauncher : MonoBehaviour
{

	[SerializeField] private int timer = 5;
	[SerializeField] private ScriptableGameEvent prepareLaunchEvent = null;
	[SerializeField] private ScriptableGameEvent launchEvent = null;
	[SerializeField] private ParticleSystem steam = null;
	[SerializeField] private Text counter = null;

	private AudioSource beep;

    private void Start()
    {
		beep = GetComponent<AudioSource>();
        StartCoroutine(LaunchSequence());
    }

    private IEnumerator LaunchSequence() {
		counter.text = "";
		yield return new WaitForSeconds(1f);

		while (timer > 0) {
 			counter.text = timer.ToString();
			beep.Play();
			yield return new WaitForSeconds(1f);
			timer--;
		}
		counter.enabled = false;
		prepareLaunchEvent.Call();
		steam.Play();

		yield return new WaitForSeconds(1f);
		launchEvent.Call();
	}
}
