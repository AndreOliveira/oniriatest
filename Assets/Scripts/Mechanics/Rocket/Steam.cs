﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class Steam : MonoBehaviour
{
    private ParticleSystem steam;

	private void Start() {
		steam = GetComponent<ParticleSystem>();
		transform.parent = null;
	}

	void OnTriggerExit(Collider other) {
		if (other.CompareTag("Rocket")) {
			ParticleSystem.EmissionModule emission = steam.emission;
			emission.enabled = false;
			Invoke("SelfDestroy", 1f);
		}
	}

	private void SelfDestroy() {
		Destroy(this.gameObject);
	}
}
