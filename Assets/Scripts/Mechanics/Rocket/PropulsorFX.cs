﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PropulsorFX
{
    private ParticleSystem[] enginesFX;
	private AudioSource engineSound;

	public PropulsorFX (ParticleSystem[] enginesFX, AudioSource engineSound) {
		this.enginesFX = enginesFX;
		this.engineSound = engineSound;
	}
	
    public void ActiveEngine () {
		engineSound.Play ();
		for (int i = 0; i < enginesFX.Length; i++) {
			enginesFX[i].Play ();
		}
	}

	public IEnumerator DisableEngine () {
		int amountEngines = enginesFX.Length;

		//initializing particles elements arrays
		float[] rates = new float[amountEngines];
		ParticleSystem.EmissionModule[] emitters = new ParticleSystem.EmissionModule[amountEngines];
		ParticleSystem.MinMaxCurve[] curves = new ParticleSystem.MinMaxCurve[amountEngines];
		for (int i = 0; i < amountEngines; i++) {
			emitters[i] = enginesFX[i].emission;
			curves[i] = enginesFX[i].emission.rateOverTime;
			rates[i] = curves[i].constant * Time.fixedDeltaTime;
		}

		// graduately decrease particles emissions and audio volume
		bool isAnyFXEnabled = true;
		while (isAnyFXEnabled) {
			isAnyFXEnabled = false;
			for (int i = 0; i < amountEngines; i++)
			{
				curves[i].constant -= rates[i];
				emitters[i].rateOverTime = curves[i];
				if (curves[i].constant > 0) {
					isAnyFXEnabled = true;
				}
			}
			engineSound.volume -= Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
		engineSound.Stop ();
	}
}
