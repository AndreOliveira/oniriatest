﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AffectedByWindZone : MonoBehaviour
{
	private Rigidbody rb;

	private void Start() {
		rb = GetComponent<Rigidbody>();
	}

    private void OnTriggerEnter(Collider other) {
		if (other.CompareTag("WindZone")) {
			PhysicsWindZone.instance.bodies.Add(rb);
		}
	}
	private void OnTriggerExit(Collider other) {
		if (other.CompareTag("WindZone")) {
			PhysicsWindZone.instance.bodies.Remove(rb);
		}
	}
}
