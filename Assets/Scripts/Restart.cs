﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Restart : MonoBehaviour
{
    public void LoadScene (string scene) {
		UnityEngine.SceneManagement.SceneManager.LoadScene(scene);
	}
}
