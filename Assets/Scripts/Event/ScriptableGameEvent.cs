﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableGameEvent", menuName = "DesafioFoguete/ScriptableGameEvent", order = 0)]
public class ScriptableGameEvent : ScriptableObject {
	private List<GameEventListener> listeners = new List<GameEventListener>();

	public void Call() {
		for (int i = 0; i < listeners.Count; i++)
		{
			listeners[i].OnEventCalled();
		}
	}

	public void AddListener(GameEventListener listener) {
		listeners.Add(listener);
	}

	public void RemoveListener(GameEventListener listener) {
		listeners.Remove(listener);
	}
} 