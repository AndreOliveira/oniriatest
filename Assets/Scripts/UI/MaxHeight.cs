﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaxHeight : MonoBehaviour
{
	[SerializeField] private ScriptableFloat scriptableHeight = null;
    [SerializeField] private Text heightValue = null;

	public void UpdateUI () {
		heightValue.text = scriptableHeight.value.ToString();
	}
	
}
