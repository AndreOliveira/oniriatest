﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
	[SerializeField] private RectTransform fillBar = null;
	[SerializeField] private RectTransform fillText = null;
	[SerializeField] private ScriptableFloat fuel = null;

	private float max;

	private void Start() {
		max = fillBar.offsetMin.y;
	}

    public void UpdateUI() {
		float current = fuel.value * max / fuel.InitialValue;
		fillBar.offsetMin = new Vector2(fillBar.offsetMin.x, current);
		fillText.offsetMin = new Vector2(fillText.offsetMin.x, current);
	}
	
}
