﻿using UnityEngine;

[CreateAssetMenu (fileName = "Float", menuName = "DesafioFoguete/Float", order = 0)]
public class ScriptableFloat : ScriptableObject, ISerializationCallbackReceiver {

	[SerializeField] private float initialValue = 0f;
	[HideInInspector] public float value = 0f;

	public void OnBeforeSerialize () {}

	public void OnAfterDeserialize () {
		value = initialValue;
	}

	public float InitialValue {
		get { return initialValue; }
	}

}